from ics import Calendar
from os import listdir, path
from appdirs import user_config_dir
from configparser import ConfigParser

config = ConfigParser()

config_dir =  user_config_dir("ics-concat")
config.read(path.join(config_dir, "config.ini"))

output_dir = config["settings"]["output_dir"]
print(output_dir)

for calendar in config.sections():
    if "dir" not in config[calendar]:
        continue
    calendar_dir = path.expanduser(config[calendar]["dir"])
    final_calendar = Calendar()
    print(calendar)

    for f in listdir(calendar_dir):
        with open(path.join(calendar_dir, f)) as event:
            c = Calendar(event.read())
            for e in c.events:
                final_calendar.events.add(e)

    with open(path.join(output_dir, f"{calendar}.ics"), "w") as file:
        file.write(str(final_calendar))
